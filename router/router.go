package router

import (
	"github.com/MuShaf-NMS/absensi/config"
	"github.com/MuShaf-NMS/absensi/handler"
	"github.com/MuShaf-NMS/absensi/middleware"
	"github.com/MuShaf-NMS/absensi/repository"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func InitializeRouter(server *gin.Engine, db *gorm.DB, config *config.Config) {
	// Define Repositories -------------------------------------------------
	agendaRepository := repository.NewAgendaRepository(db)
	absensiRepository := repository.NewAbsesniRepository(db)
	pesertRepository := repository.NewPegawaiRepository(db)
	lembagaRepository := repository.NewLembagaRepository(db)
	adminRepository := repository.NewAdminRepository(db)
	blacklistTokenRepository := repository.NewBlacklistTokenRepository(db)

	// Define Services -----------------------------------------------------
	agendaService := service.NewAgendaService(agendaRepository)
	absensiService := service.NewAbsensiService(absensiRepository)
	pegawaiService := service.NewPegawaiService(pesertRepository)
	adminService := service.NewAdminService(adminRepository)
	lembagaService := service.NewLembagaService(lembagaRepository)
	blacklistTokenService := service.NewBlacklistTokenService(blacklistTokenRepository)
	jwtService := service.NewJwtService(config.Secret_Key)

	// Define Handlers -----------------------------------------------------
	agendaHandler := handler.NewAgendaHandler(agendaService)
	absensiHandler := handler.NewAbsensiHandler(absensiService, pegawaiService)
	pegawaiHandler := handler.NewPegawaiHandler(pegawaiService, jwtService)
	lembagaHandler := handler.NewLembagaHandler(lembagaService)
	adminHandler := handler.NewAdminHandler(adminService, jwtService, blacklistTokenService)

	// Define Middlewares --------------------------------------------------
	pegawaiMiddleware := middleware.PegawaiMiddleware(jwtService, blacklistTokenService)
	adminMiddleware := middleware.AdminMiddleware(jwtService, blacklistTokenService)
	refreshAdminMiddleware := middleware.RefreshAdminMiddleware(jwtService, blacklistTokenService)
	superAdminMiddleware := middleware.SuperAdminMiddleware(adminService)

	// Define Routes -------------------------------------------------------
	server.GET("/", func(ctx *gin.Context) {
		ctx.JSON(200, map[string]string{"halo": "dunia"})
	})
	// Auth Routes
	authRouter := server.Group("/auth")
	{
		authRouter.POST("/pegawai/login", pegawaiHandler.Login)
		authRouter.POST("/admin/login", adminHandler.Login)
		authRouter.GET("/admin/refresh", refreshAdminMiddleware, adminHandler.Refresh)
	}
	// Admin Routes
	adminRoute := server.Group("/admin", adminMiddleware, superAdminMiddleware)
	// adminRoute := server.Group("/admin")
	{
		adminRoute.GET("", adminHandler.GetAll)
		adminRoute.GET("/:uuid", adminHandler.GetOne)
		adminRoute.POST("", adminHandler.CreateAdmin)
		adminRoute.PUT("", adminHandler.UpdateAdminSelf)
		adminRoute.PUT("/:uuid", adminHandler.UpdateAdmin)
		adminRoute.PUT("/password", adminHandler.UpdatePasswordSelf)
		adminRoute.DELETE("/:uuid", adminHandler.Delete)
		adminRoute.GET("/logout", adminMiddleware, adminHandler.Logout)
	}
	// Lembaga Routes
	lembagaRoute := server.Group("/lembaga", adminMiddleware)
	{
		lembagaRoute.GET("", lembagaHandler.GetAll)
		lembagaRoute.POST("", lembagaHandler.Create)
		lembagaRoute.GET("/:uuid", lembagaHandler.GetOne)
		lembagaRoute.PUT("/:uuid", lembagaHandler.Update)
		lembagaRoute.DELETE("/:uuid", lembagaHandler.Delete)
	}
	// Agenda Routes
	agendaRoute := server.Group("/agenda", adminMiddleware)
	{
		agendaRoute.GET("", agendaHandler.GetAll)
		agendaRoute.POST("", agendaHandler.Create)
		agendaRoute.GET("/:uuid", agendaHandler.GetOne)
		agendaRoute.PUT("/:uuid", agendaHandler.Update)
		agendaRoute.DELETE("/:uuid", agendaHandler.Delete)
	}
	// Absensi Routes
	// absensiRoute := server.Group("/absensi", adminMiddleware)
	absensiRoute := server.Group("/absensi")
	{
		absensiRoute.POST("/:pegawaiId", absensiHandler.Create)
		absensiRoute.GET("/:pegawaiId", absensiHandler.GetAllByPegawai)
	}
	pegawaiRoute := server.Group("/pegawai", adminMiddleware)
	{
		pegawaiRoute.GET("", pegawaiHandler.GetAll)
		pegawaiRoute.POST("", pegawaiHandler.Create)
		pegawaiRoute.GET("/:uuid", pegawaiHandler.GetOne)
		pegawaiRoute.PUT("/:uuid", pegawaiHandler.Update)
		pegawaiRoute.DELETE("/:uuid", pegawaiHandler.Delete)
		// pegawaiRoute.POST("/login", pegawaiHandler.Login)
	}
	// MobileRoute
	mobileRoute := server.Group("/mobile", pegawaiMiddleware)
	{
		absensiRoute := mobileRoute.Group("/absensi")
		{
			absensiRoute.POST("", absensiHandler.CreateSelf)
			absensiRoute.GET("", absensiHandler.GetAllByPegawaiSelf)
		}
		pegawaiRoute := mobileRoute.Group("/pegawai")
		{
			pegawaiRoute.GET("", pegawaiHandler.GetOneSelf)
		}
	}
}
