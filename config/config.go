package config

import (
	"github.com/MuShaf-NMS/absensi/helper"
)

type Config struct {
	// App_Mode   string
	App_Port   string
	DB_User    string
	DB_Pass    string
	DB_Name    string
	DB_Host    string
	DB_Port    string
	Secret_Key string
}

func GetConfig() *Config {
	LoadEnv()
	token, err := helper.Decrypt(getVariable("VAULT_TOKEN"), "<[k1o2m3ianbfco)}")
	if err != nil {
		panic(err)
	}
	v := LoadVault(token, "/secret", "kominfo", getVariable("VAULT_ADDRESS"))
	var conf = &Config{
		// App_Mode:   getVariable("APP_MODE"),
		App_Port:   v.Get("APP_PORT").(string),
		DB_User:    v.Get("DB_USER").(string),
		DB_Pass:    v.Get("DB_PASS").(string),
		DB_Name:    v.Get("DB_NAME").(string),
		DB_Host:    v.Get("DB_HOST").(string),
		DB_Port:    v.Get("DB_PORT").(string),
		Secret_Key: v.Get("SECRET_KEY").(string),
	}
	return conf
}
