package config

import (
	"context"

	"github.com/hashicorp/vault/api"
)

type vault struct {
	// Secret *api.Secret
	Secret *api.KVSecret
}

type Vault interface {
	Get(key string) interface{}
}

func LoadVault(token, path, secretPath, address string) Vault {
	client, err := api.NewClient(&api.Config{
		Address: address,
	})
	if err != nil {
		panic(err)
	}
	client.SetToken(token)
	// secret, err := client.Logical().Read(path)
	secret, err := client.KVv2(path).Get(context.Background(), secretPath)
	if err != nil {
		panic(err)
	}
	return &vault{Secret: secret}
}

func (v *vault) Get(key string) interface{} {
	value := v.Secret.Data[key]
	return value
}
