package repository

import (
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type AbsensiRepository interface {
	Create(absensi *entity.Absensi) error
	Check(pegawaiId uuid.UUID) ([]entity.Absensi, error)
	GetAllByPegawai(pegawaiId uuid.UUID) (entity.AbsensiPegawai, error)
}

type absensiRepository struct {
	db *gorm.DB
}

func (ar *absensiRepository) Create(absensi *entity.Absensi) error {
	err := ar.db.Create(absensi).Error
	return err
}

func (ar *absensiRepository) Check(pegawaiId uuid.UUID) ([]entity.Absensi, error) {
	var check []entity.Absensi
	err := ar.db.Where(&entity.Absensi{PegawaiId: pegawaiId}).Where("DATE(created_at) = CURDATE()").Find(&check).Error
	return check, err
}

func (ar *absensiRepository) GetAllByPegawai(pegawaiId uuid.UUID) (entity.AbsensiPegawai, error) {
	var absensis entity.AbsensiPegawai
	err := ar.db.Preload("Absensi").Where(&entity.AbsensiPegawai{UUID: pegawaiId}).First(&absensis).Error
	// err := ar.db.Where(&entity.AbsensiPegawai{UUID: pegawaiId}).Joins("Absensi").First(&absensis).Error
	return absensis, err
}

func NewAbsesniRepository(db *gorm.DB) AbsensiRepository {
	return &absensiRepository{
		db: db,
	}
}
