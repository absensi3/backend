package repository

import (
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type LembagaRepository interface {
	Create(lembaga *entity.Lembaga) error
	GetAll() ([]entity.Lembaga, error)
	GetOne(uuid uuid.UUID) (entity.Lembaga, error)
	Update(lembaga *entity.Lembaga, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type lembagaRepository struct {
	db *gorm.DB
}

func (ar *lembagaRepository) Create(lembaga *entity.Lembaga) error {
	err := ar.db.Create(lembaga).Error
	return err
}

func (ar *lembagaRepository) GetAll() ([]entity.Lembaga, error) {
	var res []entity.Lembaga
	err := ar.db.Find(&res).Error
	return res, err

}

func (ar *lembagaRepository) GetOne(uuid uuid.UUID) (entity.Lembaga, error) {
	var res entity.Lembaga
	err := ar.db.Where(&entity.Lembaga{UUID: uuid}).First(&res).Error
	return res, err
}

func (ar *lembagaRepository) Update(lembaga *entity.Lembaga, uuid uuid.UUID) error {
	err := ar.db.Where(&entity.Lembaga{UUID: uuid}).Updates(lembaga).Error
	return err
}

func (ar *lembagaRepository) Delete(uuid uuid.UUID) error {
	err := ar.db.Where(&entity.Lembaga{UUID: uuid}).Delete(entity.Lembaga{}).Error
	return err
}

func NewLembagaRepository(db *gorm.DB) LembagaRepository {
	return &lembagaRepository{
		db: db,
	}
}
