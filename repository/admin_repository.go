package repository

import (
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type AdminRepository interface {
	Create(admin *entity.Admin) error
	GetAll() ([]entity.Admin, error)
	GetOne(uuid uuid.UUID) (entity.Admin, error)
	GetbyUsername(username string) (entity.Admin, error)
	GetbyEmail(email string) (entity.Admin, error)
	Update(admin *entity.Admin, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type adminRepository struct {
	db *gorm.DB
}

func (ar *adminRepository) Create(admin *entity.Admin) error {
	err := ar.db.Create(admin).Error
	return err
}

func (ar *adminRepository) GetAll() ([]entity.Admin, error) {
	var res []entity.Admin
	err := ar.db.Find(&res).Error
	return res, err

}

func (ar *adminRepository) GetOne(uuid uuid.UUID) (entity.Admin, error) {
	var res entity.Admin
	err := ar.db.Where(&entity.Admin{UUID: uuid}).First(&res).Error
	return res, err
}

func (ar *adminRepository) GetbyUsername(username string) (entity.Admin, error) {
	var res entity.Admin
	err := ar.db.Where(&entity.Admin{Username: username}).First(&res).Error
	return res, err
}

func (ar *adminRepository) GetbyEmail(email string) (entity.Admin, error) {
	var res entity.Admin
	err := ar.db.Where(&entity.Admin{Email: email}).First(&res).Error
	return res, err
}

func (ar *adminRepository) Update(admin *entity.Admin, uuid uuid.UUID) error {
	err := ar.db.Where(&entity.Admin{UUID: uuid}).Updates(admin).Error
	return err
}

func (ar *adminRepository) Delete(uuid uuid.UUID) error {
	err := ar.db.Where(&entity.Admin{UUID: uuid}).Delete(entity.Admin{}).Error
	return err
}

func NewAdminRepository(db *gorm.DB) AdminRepository {
	return &adminRepository{
		db: db,
	}
}
