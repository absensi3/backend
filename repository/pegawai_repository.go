package repository

import (
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type PegawaiRepository interface {
	Create(pegawai *entity.Pegawai) error
	GetAll() ([]entity.Pegawai, error)
	GetAllPegawaiOnly() ([]dto.PegawaiOnly, error)
	GetOne(uuid uuid.UUID) (entity.Pegawai, error)
	GetByNIP(nip string) (entity.Pegawai, error)
	Update(pegawai *entity.Pegawai, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type pegawaiRepository struct {
	db *gorm.DB
}

func (pr *pegawaiRepository) Create(pegawai *entity.Pegawai) error {
	err := pr.db.Create(pegawai).Error
	return err
}

func (pr *pegawaiRepository) GetAll() ([]entity.Pegawai, error) {
	var res []entity.Pegawai
	err := pr.db.Joins("Lembaga").Find(&res).Error
	return res, err
}

func (pr *pegawaiRepository) GetAllPegawaiOnly() ([]dto.PegawaiOnly, error) {
	var res []dto.PegawaiOnly
	err := pr.db.Model(&entity.Pegawai{}).Find(&res).Error
	return res, err
}

func (pr *pegawaiRepository) GetOne(uuid uuid.UUID) (entity.Pegawai, error) {
	var res entity.Pegawai
	err := pr.db.Joins("Lembaga").Where(&entity.Pegawai{UUID: uuid}).First(&res).Error
	return res, err
}

func (pr *pegawaiRepository) Update(pegawai *entity.Pegawai, uuid uuid.UUID) error {
	err := pr.db.Where(&entity.Pegawai{UUID: uuid}).Updates(pegawai).Error
	return err
}

func (pr *pegawaiRepository) Delete(uuid uuid.UUID) error {
	err := pr.db.Where(&entity.Pegawai{UUID: uuid}).Delete(entity.Pegawai{}).Error
	return err
}

func (pr *pegawaiRepository) GetByNIP(nip string) (entity.Pegawai, error) {
	var res entity.Pegawai
	err := pr.db.Where(&entity.Pegawai{NIP: nip}).First(&res).Error
	return res, err
}

func NewPegawaiRepository(db *gorm.DB) PegawaiRepository {
	return &pegawaiRepository{
		db: db,
	}
}
