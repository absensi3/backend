package repository

import (
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type AgendaRepository interface {
	Create(agenda *entity.Agenda) error
	GetAll() ([]entity.Agenda, error)
	GetOne(uuid uuid.UUID) (entity.Agenda, error)
	Update(agenda *entity.Agenda, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type agendaRepository struct {
	db *gorm.DB
}

func (ar *agendaRepository) Create(agenda *entity.Agenda) error {
	err := ar.db.Create(agenda).Error
	return err
}

func (ar *agendaRepository) GetAll() ([]entity.Agenda, error) {
	var res []entity.Agenda
	err := ar.db.Find(&res).Error
	return res, err

}

func (ar *agendaRepository) GetOne(uuid uuid.UUID) (entity.Agenda, error) {
	var res entity.Agenda
	err := ar.db.Where(&entity.Agenda{UUID: uuid}).First(&res).Error
	return res, err
}

func (ar *agendaRepository) Update(agenda *entity.Agenda, uuid uuid.UUID) error {
	err := ar.db.Where(&entity.Agenda{UUID: uuid}).Updates(agenda).Error
	return err
}

func (ar *agendaRepository) Delete(uuid uuid.UUID) error {
	err := ar.db.Where(&entity.Agenda{UUID: uuid}).Delete(entity.Agenda{}).Error
	return err
}

func NewAgendaRepository(db *gorm.DB) AgendaRepository {
	return &agendaRepository{
		db: db,
	}
}
