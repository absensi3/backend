package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

type AdminHandler interface {
	CreateAdmin(ctx *gin.Context)
	UpdateAdmin(ctx *gin.Context)
	UpdateAdminSelf(ctx *gin.Context)
	UpdatePasswordSelf(ctx *gin.Context)
	Login(ctx *gin.Context)
	Logout(ctx *gin.Context)
	GetAll(ctx *gin.Context)
	GetOne(ctx *gin.Context)
	// GetOneSelf(ctx *gin.Context)
	Delete(ctx *gin.Context)
	Refresh(ctx *gin.Context)
}

type adminHandler struct {
	service               service.AdminService
	jwtService            service.JwtService
	blacklistTokenService service.BlacklistTokenService
}

func (ah *adminHandler) CreateAdmin(ctx *gin.Context) {
	var admin dto.CreateAdmin
	ctx.BindJSON(&admin)
	if err := helper.Validate(admin); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Create(admin); err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Create Admin success", nil)
	ctx.JSON(200, res)
}

func (ah *adminHandler) UpdateAdmin(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid lembaga_id format", nil)
		ctx.JSON(400, res)
		return
	}
	var admin dto.UpdateAdmin
	ctx.BindJSON(&admin)
	if err := helper.Validate(admin); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Update(admin, uuid); err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Update Admin success", nil)
	ctx.JSON(200, res)
}

func (ah *adminHandler) UpdateAdminSelf(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		adminID := getAdminID.(uuid.UUID)
		var admin dto.UpdateAdminSelf
		ctx.BindJSON(&admin)
		if err := helper.Validate(&admin); err != nil {
			errs := helper.ValidationError(err)
			res := helper.ErrorResponseBuilder("Validation error", errs)
			ctx.JSON(400, res)
			return
		}
		if err := ah.service.UpdateSelf(admin, adminID); err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		adminNew, err := ah.service.GetOne(adminID)
		if err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		res := helper.ResponseBuilder("Update Admin success", adminNew)
		ctx.JSON(200, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *adminHandler) UpdatePasswordSelf(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		adminID := getAdminID.(uuid.UUID)
		var password dto.UpdatePasswordSelf
		ctx.BindJSON(&password)
		if err := helper.Validate(&password); err != nil {
			errs := helper.ValidationError(err)
			res := helper.ErrorResponseBuilder("Validation error", errs)
			ctx.JSON(400, res)
			return
		}
		if err := ah.service.UpdatePasswordSelf(password, adminID); err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, nil)
			ctx.JSON(e.Code, res)
			return
		}
		res := helper.ResponseBuilder("Update Admin success", nil)
		ctx.JSON(200, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *adminHandler) Login(ctx *gin.Context) {
	var login dto.Login
	ctx.BindJSON(&login)
	if err := helper.Validate(login); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	fmt.Println(login)
	admin, err := ah.service.Login(login)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	accessToken := ah.jwtService.GenerateToken(admin.UUID, "access_token")
	data := map[string]interface{}{
		"access_token": accessToken,
		"admin":        admin,
	}
	if login.RememberMe {
		refreshToken := ah.jwtService.GenerateToken(admin.UUID, "refresh_token")
		data["refresh_token"] = refreshToken
	}
	res := helper.ResponseBuilder("Login Success", data)
	ctx.JSON(200, res)
}

func (ah *adminHandler) Logout(ctx *gin.Context) {
	if getTokenClaim, ok := ctx.Get("tokenClaim"); ok {
		if tokenClaim, ok := getTokenClaim.(jwt.MapClaims); ok {
			ah.blacklistTokenService.CreateBlacklistToken(tokenClaim["jti"].(string))
			res := helper.ResponseBuilder("Logout success", nil)
			ctx.JSON(200, res)
			return
		}
		res := helper.ErrorResponseBuilder("Unauthorized", "Token invalid")
		ctx.JSON(401, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *adminHandler) GetAll(ctx *gin.Context) {
	admins, err := ah.service.GetAll()
	if err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get All Admin success", admins)
	ctx.JSON(200, res)
}

func (ah *adminHandler) GetOne(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid admin_id format", nil)
		ctx.JSON(400, res)
		return
	}
	admin, err := ah.service.GetOne(uuid)
	if err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get One Admin success", admin)
	ctx.JSON(200, res)
}

// func (ah *adminHandler)		GetOneSelf(ctx *gin.Context)

func (ah *adminHandler) Delete(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid admin_id format", nil)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Delete(uuid); err != nil {
		e, _ := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Delete Admin success", nil)
	ctx.JSON(200, res)
}

func (ah *adminHandler) Refresh(ctx *gin.Context) {
	if getAdminID, ok := ctx.Get("adminID"); ok {
		adminID := getAdminID.(uuid.UUID)
		accessToken := ah.jwtService.GenerateToken(adminID, "access_token")
		res := helper.ResponseBuilder("Update Admin success", accessToken)
		ctx.JSON(200, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func NewAdminHandler(service service.AdminService, jwtService service.JwtService, blacklistTokenService service.BlacklistTokenService) AdminHandler {
	return &adminHandler{
		service:               service,
		jwtService:            jwtService,
		blacklistTokenService: blacklistTokenService,
	}
}
