package handler

import (
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type LembagaHandler interface {
	Create(ctx *gin.Context)
	GetAll(ctx *gin.Context)
	GetOne(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
}

type lembagaHandler struct {
	service service.LembagaService
}

func (ah *lembagaHandler) Create(ctx *gin.Context) {
	var lembaga dto.CreateLembaga
	ctx.BindJSON(&lembaga)
	if err := helper.Validate(lembaga); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Create(lembaga); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Create Lembaga Success", nil)
	ctx.JSON(200, res)
}

func (ah *lembagaHandler) GetAll(ctx *gin.Context) {
	data, err := ah.service.GetAll()
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get All Lembaga Success", data)
	ctx.JSON(200, res)
}

func (ah *lembagaHandler) GetOne(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid lembaga_id format", nil)
		ctx.JSON(400, res)
		return
	}
	data, err := ah.service.GetOne(uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get One Lembaga Success", data)
	ctx.JSON(200, res)
}

func (ah *lembagaHandler) Update(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid lembaga_id format", nil)
		ctx.JSON(400, res)
		return
	}
	var lembaga dto.UpdateLembaga
	ctx.BindJSON(&lembaga)
	if err := helper.Validate(lembaga); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err = ah.service.Update(lembaga, uuid); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Update Lembaga Success", nil)
	ctx.JSON(200, res)
}

func (ah *lembagaHandler) Delete(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid lembaga_id format", nil)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Delete(uuid); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Delete Lembaga Success", nil)
	ctx.JSON(200, res)
}

func NewLembagaHandler(service service.LembagaService) LembagaHandler {
	return &lembagaHandler{
		service: service,
	}
}
