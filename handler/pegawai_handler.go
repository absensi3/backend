package handler

import (
	"fmt"

	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type PegawaiHandler interface {
	GetAll(ctx *gin.Context)
	Create(ctx *gin.Context)
	GetOne(ctx *gin.Context)
	GetOneSelf(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
	Login(ctx *gin.Context)
}

type pegawaiHandler struct {
	service    service.PegawaiService
	jwtService service.JwtService
}

func (ph *pegawaiHandler) Create(ctx *gin.Context) {
	var pegawai dto.CreatePegawai
	ctx.BindJSON(&pegawai)
	if err := helper.Validate(pegawai); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ph.service.Create(pegawai); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Create Pegawai Success", nil)
	ctx.JSON(200, res)
}

func (ph *pegawaiHandler) GetAll(ctx *gin.Context) {
	data, err := ph.service.GetAll()
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get All Pegawai Success", data)
	ctx.JSON(200, res)
}

func (ph *pegawaiHandler) GetOne(ctx *gin.Context) {
	pegawaiIDString := ctx.Param("uuid")
	pegawaiID, err := uuid.Parse(pegawaiIDString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid pegawai_id format", nil)
		ctx.JSON(400, res)
		return
	}
	pegawai, err := ph.service.GetOne(pegawaiID)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get One Pegawai Success", pegawai)
	ctx.JSON(200, res)
}

func (ph *pegawaiHandler) GetOneSelf(ctx *gin.Context) {
	if getPegawaiID, ok := ctx.Get("pegawaiID"); ok {
		pegawaiID := getPegawaiID.(uuid.UUID)
		pegawai, err := ph.service.GetOne(pegawaiID)
		if err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, e.Errors)
			ctx.JSON(e.Code, res)
			return
		}
		res := helper.ResponseBuilder("Get One Pegawai Success", pegawai)
		ctx.JSON(200, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ph *pegawaiHandler) Update(ctx *gin.Context) {
	pegawaiIDString := ctx.Param("uuid")
	pegawaiID, err := uuid.Parse(pegawaiIDString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid pegawai_id format", nil)
		ctx.JSON(400, res)
		return
	}
	var pegawai dto.UpdatePegawai
	ctx.BindJSON(&pegawai)
	if err := helper.Validate(pegawai); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ph.service.Update(pegawai, pegawaiID); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Update Pegawai Success", nil)
	ctx.JSON(200, res)
}

func (ph *pegawaiHandler) Delete(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	fmt.Println(uuidString)
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		fmt.Println(err)
		res := helper.ErrorResponseBuilder("Invalid pegawai_id format", nil)
		ctx.JSON(400, res)
		return
	}
	if err := ph.service.Delete(uuid); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Delete Pegawai Success", nil)
	ctx.JSON(200, res)
}

func (ph *pegawaiHandler) Login(ctx *gin.Context) {
	var login dto.LoginPegawai
	ctx.BindJSON(&login)
	if err := helper.Validate(login); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	pegawai, err := ph.service.Login(login)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	token := ph.jwtService.GenerateToken(pegawai.UUID, "access_token")
	data := map[string]interface{}{
		"pegawai":      pegawai,
		"access_token": token,
	}
	res := helper.ResponseBuilder("Login Success", data)
	ctx.JSON(200, res)
}

func NewPegawaiHandler(service service.PegawaiService, jwtService service.JwtService) PegawaiHandler {
	return &pegawaiHandler{
		service:    service,
		jwtService: jwtService,
	}
}
