package handler

import (
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type AbsensiHandler interface {
	Create(ctx *gin.Context)
	CreateSelf(ctx *gin.Context)
	GetAllByPegawai(ctx *gin.Context)
	GetAllByPegawaiSelf(ctx *gin.Context)
}

type absensiHandler struct {
	service        service.AbsensiService
	pegawaiService service.PegawaiService
}

func (ah *absensiHandler) Create(ctx *gin.Context) {
	strPegawaiId := ctx.Param("pegawaiId")
	pegawaiID, err := uuid.Parse(strPegawaiId)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid pegawaiId", nil)
		ctx.JSON(400, res)
		return
	}
	err = ah.service.CreateByAdmin(pegawaiID)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Create Absensi Success", nil)
	ctx.JSON(200, res)
}

func (ah *absensiHandler) CreateSelf(ctx *gin.Context) {
	if getPegawaiId, ok := ctx.Get("pegawaiID"); ok {
		pegawaiID := getPegawaiId.(uuid.UUID)
		var absensi dto.CreateAbsensi
		ctx.BindJSON(&absensi)
		if err := helper.Validate(absensi); err != nil {
			errs := helper.ValidationError(err)
			res := helper.ErrorResponseBuilder("Validation error", errs)
			ctx.JSON(400, res)
			return
		}
		pegawai, err := ah.pegawaiService.GetOne(pegawaiID)
		if err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, e.Errors)
			ctx.JSON(e.Code, res)
			return
		}
		err = ah.service.Create(absensi, pegawai.Lembaga, pegawaiID)
		if err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, e.Errors)
			ctx.JSON(e.Code, res)
			return
		}
		res := helper.ResponseBuilder("Create Absensi Success", nil)
		ctx.JSON(200, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func (ah *absensiHandler) GetAllByPegawai(ctx *gin.Context) {
	strPegawaiId := ctx.Param("pegawaiId")
	pegawaiId, err := uuid.Parse(strPegawaiId)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid pegawaiId", nil)
		ctx.JSON(400, res)
		return
	}
	absensi, err := ah.service.GetAllByPegawai(pegawaiId)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get Absensi success", absensi)
	ctx.JSON(200, res)
}

func (ah *absensiHandler) GetAllByPegawaiSelf(ctx *gin.Context) {
	if getPegawaiId, ok := ctx.Get("pegawaiID"); ok {
		pegawaiId := getPegawaiId.(uuid.UUID)
		absensi, err := ah.service.GetAllByPegawai(pegawaiId)
		if err != nil {
			e := err.(*helper.CustomError)
			res := helper.ErrorResponseBuilder(e.Message, e.Errors)
			ctx.JSON(e.Code, res)
			return
		}
		res := helper.ResponseBuilder("Get Absensi success", absensi)
		ctx.JSON(200, res)
		return
	}
	res := helper.ErrorResponseBuilder("Unauthorized", nil)
	ctx.JSON(401, res)
}

func NewAbsensiHandler(service service.AbsensiService, pegawaiSerice service.PegawaiService) AbsensiHandler {
	return &absensiHandler{
		service:        service,
		pegawaiService: pegawaiSerice,
	}
}
