package handler

import (
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type AgendaHandler interface {
	Create(ctx *gin.Context)
	GetAll(ctx *gin.Context)
	GetOne(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
}

type agendaHandler struct {
	service service.AgendaService
}

func (ah *agendaHandler) Create(ctx *gin.Context) {
	var agenda dto.CreateAgenda
	ctx.BindJSON(&agenda)
	if err := helper.Validate(agenda); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Create(agenda); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Create Agenda Success", nil)
	ctx.JSON(200, res)
}

func (ah *agendaHandler) GetAll(ctx *gin.Context) {
	data, err := ah.service.GetAll()
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get All Agenda Success", data)
	ctx.JSON(200, res)
}

func (ah *agendaHandler) GetOne(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid user_id format", nil)
		ctx.JSON(400, res)
		return
	}
	data, err := ah.service.GetOne(uuid)
	if err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Get One Agenda Success", data)
	ctx.JSON(200, res)
}

func (ah *agendaHandler) Update(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid user_id format", nil)
		ctx.JSON(400, res)
		return
	}
	var agenda dto.UpdateAgenda
	ctx.BindJSON(&agenda)
	if err := helper.Validate(agenda); err != nil {
		errs := helper.ValidationError(err)
		res := helper.ErrorResponseBuilder("Validation error", errs)
		ctx.JSON(400, res)
		return
	}
	if err = ah.service.Update(agenda, uuid); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Update Agenda Success", nil)
	ctx.JSON(200, res)
}

func (ah *agendaHandler) Delete(ctx *gin.Context) {
	uuidString := ctx.Param("uuid")
	uuid, err := uuid.Parse(uuidString)
	if err != nil {
		res := helper.ErrorResponseBuilder("Invalid agenda_id format", nil)
		ctx.JSON(400, res)
		return
	}
	if err := ah.service.Delete(uuid); err != nil {
		e := err.(*helper.CustomError)
		res := helper.ErrorResponseBuilder(e.Message, e.Errors)
		ctx.JSON(e.Code, res)
		return
	}
	res := helper.ResponseBuilder("Delete Agenda Success", nil)
	ctx.JSON(200, res)
}

func NewAgendaHandler(service service.AgendaService) AgendaHandler {
	return &agendaHandler{
		service: service,
	}
}
