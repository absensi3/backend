package service

import (
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

type JwtCustomClaim struct {
	Identity  interface{} `json:"identity"`
	GrantType string      `json:"grant_type"`
	jwt.RegisteredClaims
}

type JwtService interface {
	GenerateToken(identity interface{}, grantType string) string
	ExtractToken(tokenString string) (*jwt.Token, error)
}

type jwtService struct {
	secretKey string
}

func (js *jwtService) GenerateToken(identity interface{}, grantType string) string {
	issued := time.Now()
	var exp time.Time
	if grantType == "access_token" {
		exp = issued.Add(time.Hour * 3)
	} else {
		exp = issued.Add(time.Hour * 24 * 30)
	}
	claim := JwtCustomClaim{
		identity,
		grantType,
		jwt.RegisteredClaims{
			ID:        uuid.NewString(),
			Issuer:    "Local",
			ExpiresAt: jwt.NewNumericDate(exp),
			IssuedAt:  jwt.NewNumericDate(issued),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenString, _ := token.SignedString([]byte(js.secretKey))
	return tokenString
}

func (js *jwtService) ExtractToken(tokenString string) (*jwt.Token, error) {
	return jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method %v", t.Header["alg"])
		}
		return []byte(js.secretKey), nil
	})
}

func NewJwtService(secretKey string) JwtService {
	return &jwtService{
		secretKey: secretKey,
	}
}
