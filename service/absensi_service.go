package service

import (
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/repository"
	"github.com/google/uuid"
)

type AbsensiService interface {
	Create(absensi dto.CreateAbsensi, lembaga entity.Lembaga, pegawaiID uuid.UUID) error
	CreateByAdmin(pegawaiID uuid.UUID) error
	GetAllByPegawai(pegawaiId uuid.UUID) (entity.AbsensiPegawai, error)
}

type absensiService struct {
	repository repository.AbsensiRepository
}

func (as *absensiService) Create(absensi dto.CreateAbsensi, lembaga entity.Lembaga, pegawaiID uuid.UUID) error {
	distance := helper.Distance(helper.Location{
		Latitude:  lembaga.Lat,
		Longitude: lembaga.Long,
	}, helper.Location{
		Latitude:  absensi.Lat,
		Longitude: absensi.Long,
	})
	if distance <= lembaga.Radius {

		absensiCreate := entity.Absensi{
			PegawaiId: pegawaiID,
			// AgendaId:  agendatId,
		}
		res, err := as.repository.Check(pegawaiID)
		if err != nil {
			customErr := helper.NewError(422, "Unprocessable entity", nil)
			return customErr
		}
		if len(res) > 0 {
			customErr := helper.NewError(403, "Same request repeated!", nil)
			return customErr
		}
		err = as.repository.Create(&absensiCreate)
		if err != nil {
			customErr := helper.NewError(422, "Unprocessable entity", nil)
			return customErr
		}
		return nil
	}
	customErr := helper.NewError(400, "Location out of range", nil)
	return customErr
}

func (as *absensiService) CreateByAdmin(pegawaiID uuid.UUID) error {
	absensiCreate := entity.Absensi{
		PegawaiId: pegawaiID,
		// AgendaId:  agendatId,
	}
	res, err := as.repository.Check(pegawaiID)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	if len(res) > 0 {
		customErr := helper.NewError(403, "Same request repeated!", nil)
		return customErr
	}
	err = as.repository.Create(&absensiCreate)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *absensiService) GetAllByPegawai(pegawaiId uuid.UUID) (entity.AbsensiPegawai, error) {
	res, err := as.repository.GetAllByPegawai(pegawaiId)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func NewAbsensiService(repository repository.AbsensiRepository) AbsensiService {
	return &absensiService{
		repository: repository,
	}
}
