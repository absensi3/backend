package service

import (
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/repository"
	"github.com/google/uuid"
)

type LembagaService interface {
	Create(lembaga dto.CreateLembaga) error
	GetAll() ([]entity.Lembaga, error)
	GetOne(uuid uuid.UUID) (entity.Lembaga, error)
	Update(lembaga dto.UpdateLembaga, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type lembagaService struct {
	repository repository.LembagaRepository
}

func (as *lembagaService) Create(lembaga dto.CreateLembaga) error {
	lembagaCreate := entity.Lembaga{
		Nama: lembaga.Nama,
		// Tingkat: lembaga.Tingkat,
		Alamat: lembaga.Alamat,
		Lat:    lembaga.Lat,
		Long:   lembaga.Long,
		Radius: lembaga.Radius,
	}
	if err := as.repository.Create(&lembagaCreate); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *lembagaService) GetAll() ([]entity.Lembaga, error) {
	res, err := as.repository.GetAll()
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func (as *lembagaService) GetOne(uuid uuid.UUID) (entity.Lembaga, error) {
	res, err := as.repository.GetOne(uuid)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func (as *lembagaService) Update(lembaga dto.UpdateLembaga, uuid uuid.UUID) error {
	lembagaUpdate := entity.Lembaga{
		Nama: lembaga.Nama,
		// Tingkat: lembaga.Tingkat,
		Alamat: lembaga.Alamat,
		Lat:    lembaga.Lat,
		Long:   lembaga.Long,
		Radius: lembaga.Radius,
	}
	if err := as.repository.Update(&lembagaUpdate, uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *lembagaService) Delete(uuid uuid.UUID) error {
	if err := as.repository.Delete(uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func NewLembagaService(repository repository.LembagaRepository) LembagaService {
	return &lembagaService{
		repository: repository,
	}
}
