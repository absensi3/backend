package service

import (
	"errors"
	"time"

	"github.com/MuShaf-NMS/absensi/custom_types"
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/repository"
	"github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type PegawaiService interface {
	Create(pegawai dto.CreatePegawai) error
	GetAll() ([]entity.Pegawai, error)
	GetOne(uuid uuid.UUID) (entity.Pegawai, error)
	Update(pegawai dto.UpdatePegawai, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
	Login(dto.LoginPegawai) (entity.Pegawai, error)
}

type pegawaiService struct {
	repository repository.PegawaiRepository
}

func (ps *pegawaiService) Create(pegawai dto.CreatePegawai) error {
	pegawaiCreate := entity.Pegawai{
		NIP:    pegawai.NIP,
		Nama:   pegawai.Nama,
		Gender: pegawai.Gender,
		TLahir: pegawai.TLahir,
		// TglLahir: datatypes.Date(tglLahir),
		Alamat: pegawai.Alamat,
		NoTelp: pegawai.NoTelp,
		Email:  pegawai.Email,
		Satgas: pegawai.Satgas,
		// Lembaga: pegawai.Lembaga,
	}
	tglLahir, err := time.Parse("2006-01-02", pegawai.TglLahir)
	if err != nil {
		customErr := helper.NewError(400, "Invalid tgl_lahir format", nil)
		return customErr
	}
	pegawaiCreate.TglLahir = custom_types.Date(tglLahir)
	lembagaId, err := uuid.Parse(pegawai.LembagaId)
	if err != nil {
		customErr := helper.NewError(400, "Invalid lembaga_id format", nil)
		return customErr
	}
	pegawaiCreate.LembagaId = lembagaId
	hashPass, err := bcrypt.GenerateFromPassword([]byte(pegawai.Password), 10)
	if err != nil {
		customErr := helper.NewError(422, "Failed to hash Password", nil)
		return customErr
	}
	pegawaiCreate.Password = string(hashPass)
	err = ps.repository.Create(&pegawaiCreate)
	if err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			customErr := helper.NewError(400, "NIP atau Email sudah ada. Mohon gunakan NIP atau Email yang lain.", nil)
			return customErr
		}
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (ps *pegawaiService) GetAll() ([]entity.Pegawai, error) {
	// res, err := ps.repository.GetAllPegawaiOnly()
	res, err := ps.repository.GetAll()
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, err
}

func (ps *pegawaiService) GetOne(uuid uuid.UUID) (entity.Pegawai, error) {
	res, err := ps.repository.GetOne(uuid)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, err
}

func (ps *pegawaiService) Update(pegawai dto.UpdatePegawai, pegawaiID uuid.UUID) error {
	tglLahir, err := time.Parse("2006-01-02", pegawai.TglLahir)
	if err != nil {
		customErr := helper.NewError(400, "Invalid tgl lahir format", nil)
		return customErr
	}
	lembagaId, err := uuid.Parse(pegawai.LembagaId)
	if err != nil {
		customErr := helper.NewError(400, "Invalid lembaga_id format", nil)
		return customErr
	}
	pegawaiUpdate := entity.Pegawai{
		NIP:       pegawai.NIP,
		Nama:      pegawai.Nama,
		Gender:    pegawai.Gender,
		TLahir:    pegawai.TLahir,
		TglLahir:  custom_types.Date(tglLahir),
		Alamat:    pegawai.Alamat,
		NoTelp:    pegawai.NoTelp,
		Email:     pegawai.Email,
		Satgas:    pegawai.Satgas,
		LembagaId: lembagaId,
	}
	if pegawai.Password != "" {
		hashPass, err := bcrypt.GenerateFromPassword([]byte(pegawai.Password), 10)
		if err != nil {
			customErr := helper.NewError(422, "Failed to hash Password", nil)
			return customErr
		}
		pegawaiUpdate.Password = string(hashPass)
	}
	if err := ps.repository.Update(&pegawaiUpdate, pegawaiID); err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			customErr := helper.NewError(400, "NIP atau Email sudah ada. Mohon gunakan NIP atau Email yang lain.", nil)
			return customErr
		}
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (ps *pegawaiService) Delete(uuid uuid.UUID) error {
	if err := ps.repository.Delete(uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (ps *pegawaiService) Login(login dto.LoginPegawai) (entity.Pegawai, error) {
	pegawai, err := ps.repository.GetByNIP(login.NIP)
	if err != nil {
		customErr := helper.NewError(403, "NIP or Password wrong", nil)
		return pegawai, customErr
	}
	if err = bcrypt.CompareHashAndPassword([]byte(pegawai.Password), []byte(login.Password)); err != nil {
		customErr := helper.NewError(403, "NIP or Password wrong", nil)
		return pegawai, customErr
	}
	return pegawai, nil
}

func NewPegawaiService(repository repository.PegawaiRepository) PegawaiService {
	return &pegawaiService{
		repository: repository,
	}
}
