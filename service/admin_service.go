package service

import (
	"errors"

	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/repository"
	"github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type AdminService interface {
	Create(admin dto.CreateAdmin) error
	GetAll() ([]entity.Admin, error)
	GetOne(uuid uuid.UUID) (entity.Admin, error)
	Update(admin dto.UpdateAdmin, uuid uuid.UUID) error
	UpdateSelf(password dto.UpdateAdminSelf, uuid uuid.UUID) error
	UpdatePasswordSelf(password dto.UpdatePasswordSelf, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
	Login(login dto.Login) (entity.Admin, error)
}

type adminService struct {
	repository repository.AdminRepository
}

func (as *adminService) Create(admin dto.CreateAdmin) error {
	hashPass, err := bcrypt.GenerateFromPassword([]byte(admin.Password), 10)
	if err != nil {
		customErr := helper.NewError(422, "Failed to hash Password", nil)
		return customErr
	}
	adminCreate := entity.Admin{
		Username: admin.Username,
		Password: string(hashPass),
		Email:    admin.Email,
		Super:    admin.Super,
	}
	if err := as.repository.Create(&adminCreate); err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			customErr := helper.NewError(400, "Username atau Email sudah ada. Mohon gunakan Username atau Email yang lain.", nil)
			return customErr
		}
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *adminService) GetAll() ([]entity.Admin, error) {
	res, err := as.repository.GetAll()
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func (as *adminService) GetOne(uuid uuid.UUID) (entity.Admin, error) {
	res, err := as.repository.GetOne(uuid)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func (as *adminService) Update(admin dto.UpdateAdmin, uuid uuid.UUID) error {
	adminUpdate := entity.Admin{
		Username: admin.Username,
		Email:    admin.Email,
		Super:    admin.Super,
	}
	if admin.Password != "" {
		hashPass, err := bcrypt.GenerateFromPassword([]byte(admin.Password), 10)
		if err != nil {
			customErr := helper.NewError(422, "Failed to hash Password", nil)
			return customErr
		}
		adminUpdate.Password = string(hashPass)
	}
	if err := as.repository.Update(&adminUpdate, uuid); err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			customErr := helper.NewError(400, "Username atau Email sudah ada. Mohon gunakan Username atau Email yang lain.", nil)
			return customErr
		}
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *adminService) UpdateSelf(admin dto.UpdateAdminSelf, uuid uuid.UUID) error {
	adminUpdate := entity.Admin{
		// Username: admin.Username,
		Email: admin.Email,
	}
	if err := as.repository.Update(&adminUpdate, uuid); err != nil {
		var mysqlErr *mysql.MySQLError
		if errors.As(err, &mysqlErr) && mysqlErr.Number == 1062 {
			customErr := helper.NewError(400, "Username atau Email sudah ada. Mohon gunakan Username atau Email yang lain.", nil)
			return customErr
		}
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *adminService) UpdatePasswordSelf(password dto.UpdatePasswordSelf, uuid uuid.UUID) error {
	admin, err := as.GetOne(uuid)
	if err != nil {
		return nil
	}
	if err := bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(password.OldPassword)); err != nil {
		customErr := helper.NewError(403, "Old Password wrong", nil)
		return customErr
	}
	hashPass, err := bcrypt.GenerateFromPassword([]byte(password.NewPassword), 10)
	if err != nil {
		customErr := helper.NewError(422, "Failed to hash Password", nil)
		return customErr
	}
	adminUpdate := entity.Admin{
		Password: string(hashPass),
	}
	if err := as.repository.Update(&adminUpdate, uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *adminService) Delete(uuid uuid.UUID) error {
	if err := as.repository.Delete(uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *adminService) Login(login dto.Login) (entity.Admin, error) {
	admin, err := as.repository.GetbyUsername(login.Username)
	if err != nil {
		customErr := helper.NewError(403, "Username or Password wrong", nil)
		return admin, customErr
	}
	if err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(login.Password)); err != nil {
		customErr := helper.NewError(403, "Username or Password wrong", nil)
		return admin, customErr
	}
	return admin, nil
}

func NewAdminService(repository repository.AdminRepository) AdminService {
	return &adminService{
		repository: repository,
	}
}
