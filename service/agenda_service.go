package service

import (
	"time"

	"github.com/MuShaf-NMS/absensi/custom_types"
	"github.com/MuShaf-NMS/absensi/dto"
	"github.com/MuShaf-NMS/absensi/entity"
	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/repository"
	"github.com/google/uuid"
	"gorm.io/datatypes"
)

type AgendaService interface {
	Create(agenda dto.CreateAgenda) error
	GetAll() ([]entity.Agenda, error)
	GetOne(uuid uuid.UUID) (entity.Agenda, error)
	Update(agenda dto.UpdateAgenda, uuid uuid.UUID) error
	Delete(uuid uuid.UUID) error
}

type agendaService struct {
	repository repository.AgendaRepository
}

func (as *agendaService) Create(agenda dto.CreateAgenda) error {
	errs := []string{}
	tanggal, err := time.Parse("2006-01-02", agenda.Tanggal)
	if err != nil {
		errs = append(errs, "Invalid tanggal date format")
	}
	waktu, err := time.Parse("15:04:05", agenda.Waktu)
	if err != nil {
		errs = append(errs, "Invalid waktu time format")
	}
	// sampai, err := time.Parse("15:04:05", agenda.Sampai)
	// if err != nil {
	// 	errs = append(errs, "Invalid sampai time format")
	// }
	if len(errs) > 0 {
		customErr := helper.NewError(400, "Invalid format", errs)
		return customErr
	}
	agendaCreate := entity.Agenda{
		Nama:    agenda.Nama,
		Tanggal: custom_types.Date(tanggal),
		Waktu:   datatypes.NewTime(waktu.Hour(), waktu.Minute(), waktu.Second(), waktu.Nanosecond()),
		// Sampai:  datatypes.NewTime(sampai.Hour(), sampai.Minute(), sampai.Second(), sampai.Nanosecond()),
	}
	if err := as.repository.Create(&agendaCreate); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *agendaService) GetAll() ([]entity.Agenda, error) {
	res, err := as.repository.GetAll()
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func (as *agendaService) GetOne(uuid uuid.UUID) (entity.Agenda, error) {
	res, err := as.repository.GetOne(uuid)
	if err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return res, customErr
	}
	return res, nil
}

func (as *agendaService) Update(agenda dto.UpdateAgenda, uuid uuid.UUID) error {
	errs := []string{}
	tanggal, err := time.Parse("2006-01-02", agenda.Tanggal)
	if err != nil {
		errs = append(errs, "Invalid tanggal date format")
	}
	waktu, err := time.Parse("15:04:05", agenda.Waktu)
	if err != nil {
		errs = append(errs, "Invalid waktu time format")
	}
	// sampai, err := time.Parse("15:04:05", agenda.Sampai)
	// if err != nil {
	// 	errs = append(errs, "Invalid sampai time format")
	// }
	if len(errs) > 0 {
		customErr := helper.NewError(400, "Invalid format", errs)
		return customErr
	}
	agendaUpdate := entity.Agenda{
		Nama:    agenda.Nama,
		Tanggal: custom_types.Date(tanggal),
		Waktu:   datatypes.NewTime(waktu.Hour(), waktu.Minute(), waktu.Second(), waktu.Nanosecond()),
		// Sampai:  datatypes.NewTime(sampai.Hour(), sampai.Minute(), sampai.Second(), sampai.Nanosecond()),
	}
	if err := as.repository.Update(&agendaUpdate, uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func (as *agendaService) Delete(uuid uuid.UUID) error {
	if err := as.repository.Delete(uuid); err != nil {
		customErr := helper.NewError(422, "Unprocessable entity", nil)
		return customErr
	}
	return nil
}

func NewAgendaService(repository repository.AgendaRepository) AgendaService {
	return &agendaService{
		repository: repository,
	}
}
