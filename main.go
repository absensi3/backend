package main

import (
	"github.com/MuShaf-NMS/absensi/config"
	"github.com/MuShaf-NMS/absensi/db"
	"github.com/MuShaf-NMS/absensi/router"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	config := config.GetConfig()
	db := db.CreateConnection(config)
	gin.SetMode(gin.ReleaseMode)
	server := gin.Default()
	cors := cors.New(cors.Config{
		// AllowAllOrigins: true,
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	})
	server.Use(cors)
	router.InitializeRouter(server, db, config)
	server.Run(":" + config.App_Port)
}
