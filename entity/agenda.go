package entity

import (
	"time"

	"github.com/MuShaf-NMS/absensi/custom_types"
	"github.com/google/uuid"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Agenda struct {
	Id        uint              `gorm:"primariKey;autoIncrement" json:"-"`
	UUID      uuid.UUID         `gorm:"type:char(36);not null" json:"uuid"`
	Nama      string            `gorm:"size:150" json:"nama"`
	Tanggal   custom_types.Date `gorm:"date" json:"tanggal"`
	Waktu     datatypes.Time    `gorm:"time" json:"waktu"`
	CreatedAt time.Time         `json:"created_at"`
	UpdatedAt time.Time         `json:"updated_at"`
}

func (agenda *Agenda) BeforeCreate(scope *gorm.DB) error {
	var err error
	newUUID, err := uuid.NewRandom()
	if err == nil {
		agenda.UUID = newUUID
	}
	return err
}
