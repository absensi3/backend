package entity

import (
	"time"

	"github.com/MuShaf-NMS/absensi/custom_types"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Pegawai struct {
	Id        uint              `gorm:"primariKey;autoIncrement" json:"-"`
	UUID      uuid.UUID         `gorm:"type:char(36);not null;unique" json:"uuid"`
	NIP       string            `gorm:"size:20;unique;not null" json:"nip"`
	Password  string            `gorm:"size:150" json:"-"`
	Nama      string            `gorm:"size:150" json:"nama"`
	Gender    string            `gorm:"type:char(1);not null" json:"gender"`
	TLahir    string            `gorm:"size:150" json:"t_lahir"`
	TglLahir  custom_types.Date `gorm:"date" json:"tgl_lahir"`
	Alamat    string            `gorm:"255" json:"alamat"`
	NoTelp    string            `gorm:"size:15" json:"no_telp"`
	Email     string            `gorm:"size:150;unique" json:"email"`
	Satgas    string            `gorm:"size:10" json:"satgas"`
	LembagaId uuid.UUID         `gorm:"type:char(36);not null" json:"lembaga_id"`
	Lembaga   Lembaga           `gorm:"foreignKey:LembagaId;references:UUID" json:"lembaga"`
	Absensi   []Absensi         `gorm:"foreignKey:PegawaiId;references:UUID" json:"absensi"`
	CreatedAt time.Time         `json:"created_at"`
	UpdatedAt time.Time         `json:"updated_at"`
}

func (pegawai *Pegawai) BeforeCreate(scope *gorm.DB) error {
	var err error
	newUUID, err := uuid.NewRandom()
	if err == nil {
		pegawai.UUID = newUUID
	}
	return err
}

type AbsensiPegawai struct {
	Id      uint      `gorm:"primariKey;autoIncrement" json:"-"`
	UUID    uuid.UUID `gorm:"type:char(36);not null;unique" json:"uuid"`
	NIP     string    `gorm:"size:20;unique;not null" json:"nip"`
	Nama    string    `gorm:"size:150" json:"nama"`
	Absensi []Absensi `gorm:"foreignKey:PegawaiId;references:UUID" json:"absensi"`
}

func (AbsensiPegawai) TableName() string {
	return "pegawai"
}
