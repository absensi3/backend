package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Admin struct {
	Id        uint      `gorm:"primariKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"type:char(36);not null;unique" json:"uuid"`
	Username  string    `gorm:"size:150;unique" json:"username"`
	Password  string    `gorm:"size:150" json:"-"`
	Email     string    `gorm:"size:150;unique" json:"email"`
	Super     bool      `gorm:"bool;default:false" json:"super"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (admin *Admin) BeforeCreate(scope *gorm.DB) error {
	var err error
	newUUID, err := uuid.NewRandom()
	if err == nil {
		admin.UUID = newUUID
	}
	return err
}
