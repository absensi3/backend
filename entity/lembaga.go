package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Lembaga struct {
	Id        uint      `gorm:"primariKey;autoIncrement" json:"-"`
	UUID      uuid.UUID `gorm:"type:char(36);not null;unique" json:"uuid"`
	Nama      string    `gorm:"size:150" json:"nama"`
	Alamat    string    `gorm:"size:255" json:"alamat"`
	Lat       float64   `gorm:"type:float" json:"lat"`
	Long      float64   `gorm:"type:float" json:"long"`
	Radius    float64   `gorm:"type:float" json:"radius"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (lembaga *Lembaga) BeforeCreate(scope *gorm.DB) error {
	var err error
	newUUID, err := uuid.NewRandom()
	if err == nil {
		lembaga.UUID = newUUID
	}
	return err
}
