package entity

import (
	"time"

	"github.com/google/uuid"
)

type Absensi struct {
	Id        uint      `gorm:"primariKey;autoIncrement" json:"-"`
	PegawaiId uuid.UUID `gorm:"type:char(36);not null" json:"pegawai"`
	CreatedAt time.Time `json:"created_at"`
}
