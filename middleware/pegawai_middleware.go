package middleware

import (
	"strings"

	"github.com/MuShaf-NMS/absensi/helper"
	"github.com/MuShaf-NMS/absensi/service"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"github.com/google/uuid"
)

func PegawaiMiddleware(jwtService service.JwtService, blacklistTokenService service.BlacklistTokenService) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		auth := ctx.Request.Header.Get("Authorization")
		if len(auth) == 0 {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		splitAuth := strings.Split(auth, " ")
		if len(splitAuth) < 2 {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		if splitAuth[0] != "Bearer" {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		tokenString := splitAuth[1]
		token, err := jwtService.ExtractToken(tokenString)
		if err != nil || !token.Valid {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		claim := token.Claims.(jwt.MapClaims)
		if blacklistTokenService.CheckBlacklistToken(claim["jti"].(string)) {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		pegawaiId, err := uuid.Parse(claim["identity"].(string))
		grantType := claim["grant_type"]
		if err != nil || grantType != "access_token" {
			res := helper.ErrorResponseBuilder("Unauthorized", nil)
			ctx.AbortWithStatusJSON(401, res)
			return
		}
		ctx.Set("pegawaiID", pegawaiId)
		ctx.Set("tokenClaim", claim)
		ctx.Next()
	}
}
