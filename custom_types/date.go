package custom_types

import (
	"database/sql"
	"database/sql/driver"
	"errors"
	"time"
)

type Date time.Time

func (date Date) MarshalText() ([]byte, error) {
	if y := time.Time(date).Year(); y < 0 || y >= 10000 {
		return nil, errors.New("Time.MarshalText: year outside of range [0,9999]")
	}
	b := make([]byte, 0, len("2006-01-02"))
	return time.Time(date).AppendFormat(b, "2006-01-02"), nil
}

func (date *Date) UnmarshalText(bytes []byte) error {
	var err error
	t, err := time.Parse("2006-01-02", time.Time(*date).Format("2006-01-02"))
	*date = Date(t)
	return err
}

func (date Date) MarshalJSON() ([]byte, error) {
	return []byte(`"` + time.Time(date).Format("2006-01-02") + `"`), nil
}

func (date *Date) UnmarshalJSON(bytes []byte) error {
	if string(bytes) == "null" {
		return nil
	}
	var err error
	t, err := time.Parse(`"`+"2006-01-02"+`"`, string(bytes))
	*date = Date(t)
	return err
}

func (date *Date) Scan(value interface{}) (err error) {
	nullTime := &sql.NullTime{}
	err = nullTime.Scan(value)
	*date = Date(nullTime.Time)
	return
}

func (date Date) Value() (driver.Value, error) {
	y, m, d := time.Time(date).Date()
	return time.Date(y, m, d, 0, 0, 0, 0, time.Time(date).Location()), nil
}
