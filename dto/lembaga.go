package dto

type CreateLembaga struct {
	Nama   string  `json:"nama" validate:"required"`
	Alamat string  `json:"alamat" validate:"required"`
	Lat    float64 `json:"lat" validate:"required"`
	Long   float64 `json:"long" validate:"required"`
	Radius float64 `json:"radius" validate:"required"`
}

type UpdateLembaga struct {
	Nama   string  `json:"nama" validate:"required"`
	Alamat string  `json:"alamat" validate:"required"`
	Lat    float64 `json:"lat" validate:"required"`
	Long   float64 `json:"long" validate:"required"`
	Radius float64 `json:"radius" validate:"required"`
}
