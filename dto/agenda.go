package dto

type CreateAgenda struct {
	Nama    string `json:"nama" validate:"required"`
	Tanggal string `json:"tanggal" validate:"required,datetime=2006-01-02"`
	Waktu   string `json:"waktu" validate:"required,datetime=15:04:05"`
}

type UpdateAgenda struct {
	Nama    string `json:"nama" validate:"required"`
	Tanggal string `json:"tanggal" validate:"required,datetime=2006-01-02"`
	Waktu   string `json:"waktu" validate:"required,datetime=15:04:05"`
}
