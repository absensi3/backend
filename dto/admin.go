package dto

type CreateAdmin struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
	Super    bool   `json:"super"`
}

type UpdateAdmin struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password"`
	Email    string `json:"email" validate:"required,email"`
	Super    bool   `json:"super"`
}

type UpdateAdminSelf struct {
	Username string `json:"username" validate:"required"`
	Email    string `json:"email" validate:"required,email"`
}

type UpdatePasswordSelf struct {
	OldPassword string `json:"old_password" validate:"required"`
	NewPassword string `json:"new_password" validate:"required"`
}

type Login struct {
	Username   string `json:"username" validate:"required"`
	Password   string `json:"password" validate:"required"`
	RememberMe bool   `json:"remember_me"`
}
