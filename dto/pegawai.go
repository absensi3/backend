package dto

import (
	"time"

	"github.com/MuShaf-NMS/absensi/custom_types"
	"github.com/google/uuid"
)

type CreatePegawai struct {
	NIP       string `json:"nip" validate:"required"`
	Nama      string `json:"nama" validate:"required"`
	Password  string `json:"password" validate:"required"`
	Gender    string `json:"gender" validate:"required"`
	TLahir    string `json:"t_lahir" validate:"required"`
	TglLahir  string `json:"tgl_lahir" validate:"required,datetime=2006-01-02"`
	Alamat    string `json:"alamat" validate:"required"`
	NoTelp    string `json:"no_telp" validate:"required"`
	Email     string `json:"email" validate:"required,email"`
	Satgas    string `json:"satgas" validate:"required"`
	LembagaId string `json:"lembaga_id" validate:"required,uuid4_rfc4122"`
}

type UpdatePegawai struct {
	NIP       string `json:"nip" validate:"required"`
	Nama      string `json:"nama" validate:"required"`
	Password  string `json:"password"`
	Gender    string `json:"gender" validate:"required"`
	TLahir    string `json:"t_lahir" validate:"required"`
	TglLahir  string `json:"tgl_lahir" validate:"required,datetime=2006-01-02"`
	Alamat    string `json:"alamat" validate:"required"`
	NoTelp    string `json:"no_telp" validate:"required"`
	Email     string `json:"email" validate:"required,email"`
	Satgas    string `json:"satgas" validate:"required"`
	LembagaId string `json:"lembaga_id" validate:"required,uuid4_rfc4122"`
}

type PegawaiOnly struct {
	UUID      uuid.UUID         `json:"uuid"`
	NIP       string            `json:"nip"`
	Nama      string            `json:"nama"`
	Gender    string            `json:"gender"`
	TLahir    string            `json:"t_lahir"`
	TglLahir  custom_types.Date `json:"tgl_lahir"`
	Alamat    string            `json:"alamat"`
	NoTelp    string            `json:"no_telp"`
	Email     string            `json:"email"`
	Satgas    string            `json:"satgas"`
	LembagaId uuid.UUID         `json:"lembaga_id"`
	CreatedAt time.Time         `json:"created_at"`
	UpdatedAt time.Time         `json:"updated_at"`
}

type LoginPegawai struct {
	NIP      string `json:"nip" validate:"required"`
	Password string `json:"password" validate:"required"`
}
