package helper

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/base64"
	"errors"
)

func Decrypt(ciphertext, password string) (string, error) {
	const blocksize = 32

	h := sha256.New()
	h.Write([]byte(password))
	key := h.Sum(nil)

	cipherText, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return "", err
	}
	iv := cipherText[0:16]
	ct := cipherText[16:]
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	mode := cipher.NewCBCDecrypter(block, iv)
	pt := make([]byte, len(ct))
	mode.CryptBlocks(pt, ct)
	dataLen := len(pt)
	if dataLen == 0 {
		return "", errors.New("data is empty")
	}

	if dataLen%blocksize != 0 {
		return "", errors.New("data is not block-aligned")
	}
	padLen := int(pt[dataLen-1])
	ref := bytes.Repeat([]byte{byte(padLen)}, padLen)
	if padLen > blocksize || padLen == 0 || !bytes.HasSuffix(pt, ref) {
		return "", errors.New("invalid padding")
	}

	if err != nil {
		return "", err
	}
	return string(pt[:dataLen-padLen]), nil
}
