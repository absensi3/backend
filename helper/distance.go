package helper

import "math"

type Location struct {
	Latitude  float64
	Longitude float64
}

func Distance(a, b Location) float64 {
	var delta Location
	delta.Latitude = (a.Latitude - b.Latitude) * math.Pi / 180
	delta.Longitude = (a.Longitude - b.Longitude) * math.Pi / 180
	r := math.Pow(math.Sin(delta.Latitude/2), 2) + math.Cos(a.Latitude)*math.Cos(b.Latitude)*math.Pow(math.Sin(delta.Longitude/2), 2)*6371000
	return r
}
